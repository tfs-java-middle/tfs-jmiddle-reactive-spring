package ru.tfs.jmiddle.reactive.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table(value = "cars")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Car {

    @Id
    private Long id;
    private String model;
    private String color;
}
