package ru.tfs.jmiddle.reactive.dto;

import lombok.Data;

@Data
public class CarCreateRequest {
    private String model;
    private String color;
}
