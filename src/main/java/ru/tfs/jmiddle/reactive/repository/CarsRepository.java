package ru.tfs.jmiddle.reactive.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import ru.tfs.jmiddle.reactive.domain.Car;

public interface CarsRepository extends ReactiveCrudRepository<Car, Long> {
}
