package ru.tfs.jmiddle.reactive.controller;

import java.time.Duration;
import java.util.UUID;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/simple")
public class SimpleRestController {

    @GetMapping("/element")
    public Mono<String> element() {
        return Mono.just(UUID.randomUUID().toString());
    }

    @GetMapping("/collection")
    public Flux<Integer> collection() {
        return Flux.range(1, 10);
    }

    @GetMapping("/duration")
    public Flux<Integer> duration() {
        return Flux.range(1, 20).delayElements(Duration.ofMillis(200));
    }

    @GetMapping(path = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> stream() {
        return Flux.generate(() -> 0, (state, emitter) -> {
                emitter.next(state);
                return state + 1;
            })
            .delayElements(Duration.ofSeconds(1L))
            .map(i -> "" + i);
    }
}