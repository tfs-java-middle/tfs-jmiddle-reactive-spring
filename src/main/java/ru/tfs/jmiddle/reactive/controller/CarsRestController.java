package ru.tfs.jmiddle.reactive.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.tfs.jmiddle.reactive.domain.Car;
import ru.tfs.jmiddle.reactive.dto.CarCreateRequest;
import ru.tfs.jmiddle.reactive.repository.CarsRepository;

@RestController
@RequestMapping("/api/v1/cars")
@RequiredArgsConstructor
public class CarsRestController {

    private final CarsRepository repository;

    @GetMapping
    public Flux<Car> getCars() {
        return repository.findAll();
    }

    @GetMapping("/{id}")
    public Mono<Car> getCar(@PathVariable Long id) {
        return repository.findById(id);
    }

    @PostMapping
    public Mono<Car> createCar(@RequestBody CarCreateRequest request) {
        Car car = new Car(null, request.getModel(), request.getColor());
        return repository.save(car);
    }
}
