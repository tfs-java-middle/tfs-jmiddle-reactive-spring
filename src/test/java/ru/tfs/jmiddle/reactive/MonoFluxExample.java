package ru.tfs.jmiddle.reactive;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

public class MonoFluxExample {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Test // Подписка на flux
    public void test1() {
        Flux.just("1", "2", "3", "4")
            .subscribe(value -> log.debug("Value: {}", value));
    }

    @Test // Подписка с обработкой исключения
    public void test2() {
        Flux.just(1, 2, 3, 4, 5)
            .subscribe(value -> {
                if (value > 4) {
                    throw new IllegalArgumentException(value + " > than 4");
                }
                log.debug("Value: {}", value);
            }, error -> log.error("Error: {}", error.getMessage(), error));
    }

    @Test // Отмена подписки
    public void test3() throws Exception {
        Disposable disposable = Flux.just(1, 2, 3, 4, 5, 6, 7, 8)
            .delayElements(Duration.ofSeconds(1))
            .subscribe(value -> log.debug("Value: {}", value));

        Thread.sleep(6000);
        disposable.dispose();
        log.debug("Cancelling subscription");
    }

    @Test // Преобразование элементов
    public void test4() throws Exception {
        Flux.just(1, 2, 3, 4, 5, 6, 7, 8)
            .map(i -> i + 1)
            .subscribe(value -> log.debug("Value: {}", value));
    }

    @Test // flatmap - не гарантирует порядок элементов
    public void test5() {
        Flux.just("1,2,3", "4,5,6")
            .flatMap(i -> Flux.fromIterable(Arrays.asList(i.split(","))))
            .collect(Collectors.toList())
            .subscribe(s -> log.debug("Value: {}", s));
    }

    @Test // Распараллелить
    public void test6() {
        Flux.range(1, 10)
            .parallel(2)
            .runOn(Schedulers.parallel())
            .subscribe(i -> log.debug("{} - {}", Thread.currentThread().getName(), i));
    }

    @Test
    public void sequenceEqual() {
        Mono.sequenceEqual(
                Flux.just(1, 2, 3),
                Flux.just(1, 2, 4)
            )
            .subscribe(b -> log.debug("{}", b));
    }

    @Test
    public void defaultIfEmpty() {
        Flux.fromIterable(Collections.emptyList())
            .defaultIfEmpty(10)
            .subscribe(i -> log.debug("{}", i));
    }

    @Test
    public void merge() throws Exception {
        Flux.merge(
                Flux.just(1, 2, 3).delayElements(Duration.ofMillis(10)),
                Flux.just(10, 20, 30).delayElements(Duration.ofMillis(5))
            )
            .subscribe(i -> log.debug("{}", i));

        Thread.sleep(1000);
    }

    @Test
    public void concat() throws Exception {
        Flux.concat(
                Flux.just(1, 2, 3).delayElements(Duration.ofMillis(10)),
                Flux.just(10, 20, 30).delayElements(Duration.ofMillis(5))
            )
            .subscribe(i -> log.debug("{}", i));

        Thread.sleep(1000);
    }

    @Test
    public void zip() {
        Flux.zip(
                Flux.just(1, 2, 3, 4),
                Flux.just(10, 20, 30)
            )
            .subscribe(i -> log.debug("{}", i));
    }

    @Test
    public void distinct() {
        Flux.just(1, 2, 5, 3, 2, 5, 6)
            .distinct()
            .subscribe(i -> log.debug("{}", i));
    }

    @Test
    public void filter() {
        Flux.just(1, 2, 5, 3, 2, 5, 6)
            .filter(i -> i > 2)
            .subscribe(i -> log.debug("{}", i));
    }

    @Test
    public void take() throws Exception {
        Flux.just(1, 3, 2, 5, 4).delayElements(Duration.ofMillis(200))
            .filter(i -> i > 2)
            .take(2)
            .subscribe(i -> log.debug("{}", i));

        Thread.sleep(2000);
    }

    @Test
    public void count() {
        Flux.just(1, 2, 5, 3, 2, 5, 6)
            .count()
            .subscribe(i -> log.debug("{}", i));
    }

    @Test
    public void reduce() {
        Flux.just(1, 2, 5, 3, 2, 5, 6)
            .reduce(Integer::sum)
            .subscribe(i -> log.debug("{}", i));
    }
}
