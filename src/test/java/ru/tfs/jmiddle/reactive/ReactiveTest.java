package ru.tfs.jmiddle.reactive;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import ru.tfs.jmiddle.reactive.controller.CarsRestController;
import ru.tfs.jmiddle.reactive.domain.Car;
import ru.tfs.jmiddle.reactive.repository.CarsRepository;

@SpringBootTest
public class ReactiveTest {

    @Autowired
    private CarsRepository carsRepository;
    @Autowired
    private RouterFunction<ServerResponse> routes;
    @Autowired
    private CarsRestController controller;

    @Test
    public void repositoryTest() {
        Mono<Car> car = carsRepository.save(new Car(null, "Lada", "Black"));
        StepVerifier.create(car)
            .assertNext(Assertions::assertNotNull)
            .expectComplete()
            .verify();
    }

    @Test
    public void annotatedControllerTest() {

        WebTestClient client = WebTestClient.bindToController(controller).build();

        client.get().uri("/api/v1/cars/1")
            .exchange()
            .expectStatus().isOk()
            .expectBody().jsonPath("$.color").isEqualTo("White");
    }

    @Test
    public void functionalRouteTest() {

        WebTestClient client = WebTestClient.bindToRouterFunction(routes).build();

        client.get().uri("/api/v2/cars/2")
            .exchange()
            .expectStatus().isOk()
            .expectBody().jsonPath("$.model").isEqualTo("Toyota");
    }
}
